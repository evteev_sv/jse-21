package com.nlmk.evteev.tm.entity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Класс, описывающий проект
 */
@XmlRootElement
public class Project {

    private Long id = System.nanoTime();
    private String name = "";
    private String description = "";
    private UUID ownedUser;
    private final List<Long> tasks = new ArrayList<>();

    /**
     * Создание класса
     *
     * @param pName имя проекта
     */
    public Project(String pName) {
        name = pName;
    }

    public Long getId() {
        return id;
    }

    @XmlElement
    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    @XmlElement
    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    @XmlElement
    public void setDescription(String description) {
        this.description = description;
    }

    public List<Long> getTasks() {
        return tasks;
    }

    public UUID getOwnedUser() {
        return ownedUser;
    }

    @XmlElement
    public void setOwnedUser(UUID ownedUser) {
        this.ownedUser = ownedUser;
    }

    @Override
    public String toString() {
        return "Project {id=" + id + ",\n name=" + name + "}\n";
    }

}

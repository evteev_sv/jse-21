package com.nlmk.evteev.tm.subscriber;

import com.nlmk.evteev.tm.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.nlmk.evteev.tm.constant.TerminalConst.*;

/**
 * Класс-подписчик на команды издателя {@link Publisher}
 * <p>
 * Обрабатывает команды, касающиеся пользователей
 * <p/>
 */
public class UserSubscriber implements ISubscriber {

    private static final Logger logger = LogManager.getLogger(UserSubscriber.class);

    @Override
    public void handleNotify(String msg) {
        if (msg == null || msg.isEmpty()) {
            logger.warn("Команда пустая, выполнение прервано.");
            return;
        }
        switch (msg) {
            case USER_CREATE:
                UserService.getInstance().createUser();
                break;
            case USER_UPDATE:
                UserService.getInstance().updateUser();
                break;
            case USER_DELETE:
                UserService.getInstance().deleteUser();
                break;
            case USER_LIST:
                UserService.getInstance().printListUsers();
                break;
            case USER_LOGIN:
                UserService.getInstance().userLogin();
                break;
            case USER_VIEW:
                UserService.getInstance().viewUserProfile();
                break;
            case USER_CHANGE_PASSWORD:
                UserService.getInstance().changeUserPassword();
                break;
            case USER_PROFILE_EDIT:
                UserService.getInstance().editUserProfile();
                break;
            case USER_LOGOUT:
                UserService.getInstance().userLogOut();
                break;
            default:
                break;
        }
    }

}

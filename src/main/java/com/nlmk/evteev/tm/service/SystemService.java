package com.nlmk.evteev.tm.service;

import com.nlmk.evteev.tm.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Deque;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * Сервисный класс для обработки комманд
 */
public class SystemService {

    private final static Logger logger = LogManager.getLogger(SystemService.class);
    private static SystemService instance = null;
    private final Deque<String> deque = new LinkedBlockingDeque<>(10);

    private SystemService() {
    }

    public static SystemService getInstance() {
        synchronized (SystemService.class) {
            return instance == null
                    ? instance = new SystemService()
                    : instance;
        }
    }

    /**
     * Показ сообщения об ошибке
     */
    public void displayError() {
        System.out.println("Неподдерживаемый аргумент. наберите команду " +
                "help для получения списка доступных команд...");
    }

    /**
     * Показ сведений о ключах
     */
    public void displayHelp() {
        System.out.println("version - Информация о версии.");
        System.out.println("about - Информация о разработчике.");
        System.out.println("help - Информация о доступных командах.");
        System.out.println("exit - Завершение работы приложения");

        System.out.println("proj-create - Создание проекта");
        System.out.println("proj-clear - Очистка проектов");
        System.out.println("proj-list - Вывод списка проектов");
        System.out.println("proj-view - Просмотр проектов");
        System.out.println("proj-remove-by-name - Удаление проекта по имени");
        System.out.println("proj-remove-by-id - Удаление проекта по коду");
        System.out.println("proj-remove-by-index - Удаление проекта по индексу");
        System.out.println("proj-update-by-index - Изменение проекта по индексу");
        System.out.println("proj-update-by-id - Изменение проекта по коду");
        System.out.println("proj-add-task-by-ids - Добавление задачи к проекту по коду");
        System.out.println("proj-remove-task-by-ids - Удаление задачи из проекта");
        System.out.println("proj-remove-tasks - Удаление всех задач из проекта");
        System.out.println("proj-remove-with-tasks - Удаление проекта вместе с задачами");
        System.out.println("proj-change-owner - Изменение владельца проекта");
        System.out.println("proj-export-json - Экспорт данных в файл в формате JSON");
        System.out.println("proj-export-xml - Экспорт данных в файл в формате XML");

        System.out.println("task-create - Создание задачи");
        System.out.println("task-clear - Очистка задачи");
        System.out.println("task-list - Вывод списка задач");
        System.out.println("task-view - Просмотр задачи");
        System.out.println("task-remove-by-id - Удаление задачи по коду");
        System.out.println("task-remove-by-name - Удаление задачи по имени");
        System.out.println("task-remove-by-index - Удаление задачи по индексу");
        System.out.println("task-update-by-index - Изменение задачи по индексу");
        System.out.println("task-update-by-id - Изменение задачи по коду");
        System.out.println("task-view-without-project - Просмотр задач, непривязанных к проектам");
        System.out.println("task-view-by-project - Просмотр задач по коду проекта");

        System.out.println("user-create - Создание пользователя");
        System.out.println("user-login - Вход в систему");
        System.out.println("user-logout - Выход из системы");
        System.out.println("user-update - Обновление пользователя");
        System.out.println("user-list - Вывод списка пользователей");
        System.out.println("user-view - Просмотр профиля пользователя");
        System.out.println("user-delete - Удаление пользователя");
        System.out.println("user-change-pass - Смена пароля пользователем");
        System.out.println("user-profile-edit - Редактирование профиля пользователя");
    }

    /**
     * Показ сведений о версиях
     */
    public void displayVersion() {
        System.out.println("1.0.21");
    }

    /**
     * Показ сведений об авторе
     */
    public void displayAbout() {
        System.out.println("Author: Sergey Evteev");
        System.out.println("e-mail: sergey@evteev.ru");
    }

    /**
     * Показ приветствия
     */
    public void displayWelcome(final User user) {
        if (user == null)
            System.out.println("** WELCOME TO TASK MANAGER, GUEST **");
        else
            System.out.println("** WELCOME TO TASK MANAGER, " + user.getLoginName() + " **");
    }

    /**
     * Стандартный выход
     */
    public void displayExit() {
        System.out.println("Получена команда завершения работы...");
        System.exit(0);
    }

    /**
     * История вводимых команд
     */
    public void displayHistory() {
        System.out.println("История комманд: ");
        if (deque.isEmpty())
            System.out.println("История комманд пуста");
        deque.forEach(System.out::println);
    }

    /**
     * Запись команды в историю
     *
     * @param cmd команда
     */
    public void addToHistory(final String cmd) {
        if (deque.size() == 10) {
            deque.pollLast();
        }
        deque.offerFirst(cmd);
    }

}

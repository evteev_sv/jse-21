package com.nlmk.evteev.tm.subscriber;

import com.nlmk.evteev.tm.exceptions.TaskNotFoundException;
import com.nlmk.evteev.tm.service.TaskService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.nlmk.evteev.tm.constant.TerminalConst.*;

public class TaskSubscriber implements ISubscriber {

    private static final Logger logger = LogManager.getLogger(TaskSubscriber.class);

    @Override
    public void handleNotify(String msg) {
        if (msg == null || msg.isEmpty()) {
            logger.warn("Команда пустая. Выполнение прервано.");
            return;
        }
        switch (msg) {
            case TASK_CREATE:
                TaskService.getInstance().createTask();
                break;
            case TASK_CLEAR:
                TaskService.getInstance().clearTask();
                break;
            case TASK_LIST:
                TaskService.getInstance().listTask();
                break;
            case TASK_VIEW:
                try {
                    TaskService.getInstance().viewTaskByIndex();
                } catch (TaskNotFoundException e) {
                    logger.error("Задача не найдена! \n".concat(e.getMessage()));
                }
                break;
            case TASK_VIEW_WITHOUT_PROJECT:
                TaskService.getInstance().findAllWithoutProject();
                break;
            case TASK_VIEW_BY_PROJECT:
                TaskService.getInstance().findAllByProjectId();
                break;
            case TASK_REMOVE_BY_ID:
                TaskService.getInstance().removeTaskById();
                break;
            case TASK_REMOVE_BY_NAME:
                TaskService.getInstance().removeTaskByName();
                break;
            case TASK_REMOVE_BY_INDEX:
                TaskService.getInstance().removeTaskByIndex();
                break;
            case TASK_UPDATE_BY_ID:
                TaskService.getInstance().updateTaskById();
                break;
            case TASK_UPDATE_BY_INDEX:
                try {
                    TaskService.getInstance().updateTaskByIndex();
                } catch (TaskNotFoundException e) {
                    logger.error("Задача не найдена! \n".concat(e.getMessage()));
                }
                break;
        }
    }

}

package com.nlmk.evteev.tm.repository;

import com.nlmk.evteev.tm.entity.User;
import com.nlmk.evteev.tm.enumerations.UserRole;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Класс для работы с сущностью пользователя
 */
public class UserRepository {

    private static UserRepository instance = null;
    private final List<User> users = new ArrayList<>();

    private UserRepository() {
    }

    /**
     * Создает и возвращает экземпляр класса
     *
     * @return экземпляр класса {@link UserRepository}
     */
    public static UserRepository getInstance() {
        synchronized (UserRepository.class) {
            return instance == null
                    ? instance = new UserRepository()
                    : instance;
        }
    }

    /**
     * Создание пользователя
     *
     * @param firstName  фамилия
     * @param middleName имя
     * @param secondName отчество
     * @param loginName  логон пользователя
     * @param password   пароль
     * @param userRole   роль пользователя
     * @return созданный в системе пользователь {@link User}
     */
    public User create(
            final String loginName, final String password, final UserRole userRole,
            final String firstName, final String middleName, final String secondName
    ) {
        final User user = new User(loginName, password, userRole, firstName, middleName, secondName);
        users.add(user);
        return user;
    }

    /**
     * Создание пользователя
     *
     * @param loginName регистрационное имя
     * @param password  пароль
     * @param userRole  роль пользователя
     * @return пользователь {@link User}
     */
    public User create(final String loginName, final String password, final UserRole userRole) {
        final User user = new User();
        user.setLoginName(loginName);
        user.setPassword(password);
        user.setUserRole(userRole);
        users.add(user);
        return user;
    }

    /**
     * Изменение пользователя
     *
     * @param firstName  фамилия
     * @param middleName имя
     * @param secondName отчество
     * @param loginName  логон пользователя
     * @param password   пароль
     * @param userRole   роль пользователя
     * @return пользователь {@link User}
     */
    public User update
    (
            final String loginName, final String password, final UserRole userRole,
            final String firstName, final String middleName, final String secondName
    ) {
        User user = getUserByLoginName(loginName);
        if (user == null) return null;
        user.setFirstName(firstName);
        user.setSecondName(secondName);
        user.setMiddleName(middleName);
        user.setPassword(password);
        user.setUserRole(userRole);
        return user;
    }

    /**
     * Обновление пользователя
     *
     * @param user сущность пользователя для обновления
     * @return пользователь
     * @see User
     */
    public User update(final User user) {
        final int index = users.indexOf(user);
        if (index == -1) return null;
        users.set(index, user);
        return user;
    }

    /**
     * Возвращает пользователя по его логину
     *
     * @param loginName логин пользователя
     * @return пользователь {@link User} или NULL, если не найден
     */
    public User getUserByLoginName(final String loginName) {
        final List<User> filterUserList = users.stream().filter(user ->
                user.getLoginName().equals(loginName)).collect(Collectors.toList());
        if (filterUserList.size() == 0) return null;
        return filterUserList.get(0);
    }

    /**
     * Поиск пользователя по ID
     *
     * @param userId код пользователя
     * @return пользователь с данным ID, либо NULL, если не найден
     */
    public User getUserById(final String userId) {
        final List<User> userList = users.stream().filter(user ->
                user.getUserId().equals(UUID.fromString(userId))).collect(Collectors.toList());
        if (userList.isEmpty()) return null;
        return userList.get(0);
    }

    /**
     * Удаление пользователя
     *
     * @param loginName логин пользователя
     * @return удаленный пользователь {@link User}
     * или NULL, если такого не существует
     */
    public User deleteUserByLogin(final String loginName) {
        User user = getUserByLoginName(loginName);
        if (user == null) return null;
        users.remove(user);
        return user;
    }

    /**
     * Удаление пользователя
     *
     * @param userId логин пользователя
     * @return удаленный пользователь {@link User}
     * или NULL, если такого не существует
     */
    public User deleteUserById(final String userId) {
        User user = getUserById(userId);
        if (user == null) return null;
        users.remove(user);
        return user;
    }

    /**
     * Список пользователей
     *
     * @return список пользователей в системе {@link List}
     * @see User
     */
    public List<User> findAll() {
        return users;
    }

}

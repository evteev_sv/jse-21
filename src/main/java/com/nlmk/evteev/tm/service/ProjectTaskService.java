package com.nlmk.evteev.tm.service;

import com.nlmk.evteev.tm.entity.Project;
import com.nlmk.evteev.tm.entity.Task;
import com.nlmk.evteev.tm.enumerations.ExportType;
import com.nlmk.evteev.tm.exceptions.ProjectNotFoundException;
import com.nlmk.evteev.tm.exceptions.TaskNotFoundException;
import com.nlmk.evteev.tm.export.DataExporter;
import com.nlmk.evteev.tm.repository.ProjectRepository;
import com.nlmk.evteev.tm.repository.TaskRepository;
import com.nlmk.evteev.tm.utils.TaskManagerUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Scanner;

public class ProjectTaskService {

    private final static Logger logger = LogManager.getLogger(ProjectTaskService.class);
    private static ProjectTaskService instance = null;
    protected final Scanner scanner = new Scanner(System.in);

    private ProjectTaskService() {
    }

    public static ProjectTaskService getInstance() {
        synchronized (ProjectTaskService.class) {
            return instance == null
                    ? instance = new ProjectTaskService()
                    : instance;
        }
    }

    /**
     * Просмотр проекта вместе с задачами
     *
     * @throws ProjectNotFoundException выкидывет ошибку поиска проекта {@link ProjectNotFoundException}
     */
    public void viewProjectWithTasks() throws ProjectNotFoundException {
        logger.info("[VIEW PROJECT WITH TASKS]");
        System.out.println("Введите код проекта:");
        final Long projectId = Long.parseLong(scanner.nextLine());
        if (projectId == null) {
            logger.error("Id проекта не определено!");
            return;
        }
        Project project = ProjectService.getInstance().findProjectById(projectId);
        if (project == null) {
            throw new ProjectNotFoundException("Проект с таким ID не найден.");
        }
        System.out.println(project);
        if (project.getTasks().size() > 0) {
            System.out.println("Задачи, назначенные на проект: ");
            project.getTasks().forEach(aLong -> {
                Task task = TaskService.getInstance().findTaskById(aLong);
                if (task != null) {
                    TaskService.getInstance().viewTask(task);
                }
            });
            TaskManagerUtil.printAndReturnOk();
        } else
            TaskManagerUtil.printAndReturnFail("К данному проекту не привязана ни одна задача.");
    }


    /**
     * Добавление задачи к проекту по id
     *
     * @throws ProjectNotFoundException ошибки при поиске проекта {@link ProjectNotFoundException}
     * @throws TaskNotFoundException    ошибки при поиске задачи {@link TaskNotFoundException}
     */
    public void addTaskToProjectByIds() throws ProjectNotFoundException, TaskNotFoundException {
        logger.info("[ADD TASK TO PROJECT BY IDS]");
        System.out.println("Введите код проекта:");
        final Long projectId = Long.parseLong(scanner.nextLine());
        if (projectId == null) {
            logger.error("Код проекта не определен!");
            return;
        }
        Project project = ProjectService.getInstance().findProjectById(projectId);
        if (project == null) {
            throw new ProjectNotFoundException("Проект не найден!");
        }
        System.out.println("Введите код задачи:");
        final Long taskId = Long.parseLong(scanner.nextLine());
        if (taskId == null) {
            logger.error("Код задачи не определен!");
            return;
        }
        Task task = TaskService.getInstance().findTaskById(taskId);
        if (task == null) {
            throw new TaskNotFoundException("Задача не найдена!");
        }
        TaskRepository.getInstance().addTaskToProjectByIds(projectId, taskId);
        ProjectService.getInstance().addTaskToProject(projectId, taskId);
        TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Удаление задачи из проекта по коду
     *
     * @throws ProjectNotFoundException ошибки при поиске проекта {@link ProjectNotFoundException}
     * @throws TaskNotFoundException    ошибки при поиске задачи {@link TaskNotFoundException}
     */
    public void removeTaskFromProjectById() throws ProjectNotFoundException, TaskNotFoundException {
        logger.info("[REMOVE TASK FROM PROJECT BY IDS]");
        System.out.println("Введите код проекта:");
        final Long projectId = Long.parseLong(scanner.nextLine());
        if (projectId == null) {
            logger.error("Код проекта не определен!");
            return;
        }
        System.out.println("Введите код задачи:");
        final Long taskId = Long.parseLong(scanner.nextLine());
        if (taskId == null) {
            logger.error("Код задачи не определен!");
            return;
        }
        Project project = ProjectService.getInstance().findProjectById(projectId);
        if (project == null) {
            throw new ProjectNotFoundException("Проект не найден!");
        }
        Task task = TaskService.getInstance().findTaskById(taskId);
        if (task == null) {
            throw new TaskNotFoundException("Задача не найдена!");
        }
        TaskService.getInstance().removeTaskFromProject(taskId);
        ProjectService.getInstance().removeTaskFromProject(projectId, taskId);
        TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Очистка задач проекта
     */
    public void clearProjectTasks() {
        logger.info("[CLEAR PROJECT TASKS]");
        System.out.println("Введите код проекта:");
        final Long projectId = Long.parseLong(scanner.nextLine());
        if (projectId == null) {
            logger.error("Код проекта не определен!");
            return;
        }
        Project project = ProjectService.getInstance().findProjectById(projectId);
        if (project.getTasks().size() > 0) {
            project.getTasks().forEach(aLong -> {
                Task task = TaskService.getInstance().findTaskById(projectId);
                if (task != null) {
                    TaskService.getInstance().removeTaskFromProject(aLong);
                }
            });
            project.getTasks().clear();
        }
        TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Удаление проекта вместе с задачами
     */
    public void removeProjectWithTasks() {
        logger.info("[REMOVE PROJECT WITH TASKS]");
        System.out.println("Введите код проекта:");
        final Long projectId = Long.parseLong(scanner.nextLine());
        if (projectId == null) {
            logger.error("Код проекта не определен!");
            return;
        }
        Project project = ProjectService.getInstance().findProjectById(projectId);
        if (project.getTasks().size() > 0) {
            project.getTasks().forEach(aLong -> {
                Task task = TaskService.getInstance().findTaskById(projectId);
                if (task != null) {
                    TaskService.getInstance().removeById(aLong);
                }
            });
            ProjectService.getInstance().removeById(projectId);
        }
        TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Экспортирование данных в файл
     *
     * @param exportType тип экспорта {@link ExportType}
     */
    public void exportData(ExportType exportType) {
        DataExporter<Project> projectDataExporter = new DataExporter<>(exportType, ProjectRepository.getInstance());
        DataExporter<Task> taskDataExporter = new DataExporter<>(exportType, TaskRepository.getInstance());
        String ext = exportType == ExportType.TYPE_JSON ? ".json" : ".xml";
        try {
            projectDataExporter.exportToFile("proj_export".concat(ext));
            taskDataExporter.exportToFile("task_export".concat(ext));
            logger.info("[OK] Экспорт завершен.");
        } catch (IOException e) {
            logger.error("Ошибка экспорта данных в файл! ".concat(e.getMessage()));
        }
    }

}

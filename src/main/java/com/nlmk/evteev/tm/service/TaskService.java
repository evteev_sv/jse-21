package com.nlmk.evteev.tm.service;

import com.nlmk.evteev.tm.entity.Task;
import com.nlmk.evteev.tm.exceptions.TaskNotFoundException;
import com.nlmk.evteev.tm.repository.TaskRepository;
import com.nlmk.evteev.tm.utils.TaskManagerUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 * Сервисный класс для работы с репозиторием задач
 */
public class TaskService {

    private final Logger logger = LogManager.getLogger(TaskService.class);
    private static final TaskRepository taskRepository = TaskRepository.getInstance();
    private static TaskService instance = null;
    protected final Scanner scanner = new Scanner(System.in);

    private TaskService() {
    }

    /**
     * Создает и возвращает экземпляр класса
     *
     * @return экземпляр класса {@link TaskService}
     */
    public static TaskService getInstance() {
        synchronized (TaskService.class) {
            return instance == null
                    ? instance = new TaskService()
                    : instance;
        }
    }

    /**
     * Создание задачи и добавление ее в список задач
     *
     * @param name имя задачи
     * @return объект типа {@link Task}
     */
    private Task create(final String name) {
        if (name == null || name.isEmpty())
            return null;
        logger.trace("Создание задачи с именем: " + name);
        return taskRepository.create(name);
    }

    /**
     * Очистка списка задач
     */
    private void clear() {
        taskRepository.clear();
    }

    /**
     * Возвращает список задач
     *
     * @return список задач {@link List}
     */
    private List<Task> findAll() {
        return taskRepository.findAll();
    }

    /**
     * Поиск задачи по индексу
     *
     * @param index индекс задачи
     * @return задача {@link Task}
     */
    private Task findByIndex(final int index) throws TaskNotFoundException {
        if (index < 0) return null;
        Task task = taskRepository.findByIndex(index);
        if (task == null) {
            logger.error("Задача по данному индексу не найдена!");
            throw new TaskNotFoundException("Задача по данному индексу не найдена!");
        }
        return task;
    }

    /**
     * Поиск задачи по наименованию
     *
     * @param name наименование задачи
     * @return задача {@link Task}
     */
    private Task findByName(final String name) {
        if (name == null || name.isEmpty())
            return null;
        logger.trace("Поиск задачи с именем: " + name);
        return taskRepository.findByName(name);
    }

    /**
     * Поиск задачи по коду
     *
     * @param id колд задачи
     * @return задача {@link Task}
     */
    private Task findById(final Long id) {
        if (id == null) return null;
        logger.trace("Поиск задачи с кодом " + id);
        return taskRepository.findById(id);
    }

    /**
     * Удаление задачи по коду
     *
     * @param id код задачи
     * @return удаленная задача {@link Task}
     */
    public Task removeById(final Long id) {
        if (id == null) return null;
        return taskRepository.removeById(id);
    }

    /**
     * Удаление задачи по наименованию
     *
     * @param name наименование задачи
     * @return удаленная задача {@link Task}
     */
    private Task removeByName(final String name) {
        if (name == null || name.isEmpty())
            return null;
        return taskRepository.removeByName(name);
    }

    /**
     * Удаление задачи по индексу
     *
     * @param index индекс задачи
     * @return удаленная задача {@link Task}
     */
    private Task removeByIndex(final Integer index) {
        if (index < 0 || index > taskRepository.size() - 1) return null;
        return taskRepository.removeByIndex(index);
    }

    /**
     * Удаление задачи по названию
     */
    public void removeTaskByName() {
        logger.info("[Remove task by name]");
        System.out.println("Введите имя задачи: ");
        final String name = scanner.nextLine();
        final Task task = removeByName(name);
        if (task == null)
            TaskManagerUtil.printAndReturnFail("Удаление задачи не удалось!");
        else
            TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Удаление задачи по коду
     */
    public void removeTaskById() {
        System.out.println("[Remove task by id]");
        System.out.println("Введите ID задачи: ");
        final Long vId = scanner.nextLong();
        final Task task = removeById(vId);
        if (task == null)
            TaskManagerUtil.printAndReturnFail("Удаление задачи не удалось!");
        else
            TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Удаление задачи по индексу
     */
    public void removeTaskByIndex() {
        System.out.println("[Remove task by index]");
        System.out.println("Введите индекс задачи: ");
        final Integer vId = scanner.nextInt();
        final Task task = removeByIndex(vId);
        if (task == null)
            TaskManagerUtil.printAndReturnFail("Удаление задачи не удалось!");
        else
            TaskManagerUtil.printAndReturnOk();
    }


    /**
     * Изменение задачи
     *
     * @param id          код задачи
     * @param name        наименование задачи
     * @param description описание задачи
     * @return измененная задача {@link Task}
     */
    private Task update(final Long id, final String name, final String description) {
        if (id == null || name == null || name.isEmpty() || description == null)
            return null;
        return taskRepository.update(id, name, description);
    }


    /**
     * Изменение задачи по индексу
     */
    public void updateTaskByIndex() throws TaskNotFoundException {
        logger.info("[Update task by index]");
        System.out.println("Введите индекс задачи: ");
        final int vId = Integer.parseInt(scanner.nextLine()) - 1;
        final Task task;
        try {
            task = findByIndex(vId);
            System.out.println("Введите новое название задачи: ");
            final String name = scanner.nextLine();
            System.out.println("Введите новое описание задачи: ");
            final String description = scanner.nextLine();
            if (update(task.getId(), name, description) != null) {
                TaskManagerUtil.printAndReturnOk();
            } else {
                TaskManagerUtil.printAndReturnFail("Изменение задачи не удалось.");
            }
        } catch (TaskNotFoundException e) {
            logger.error(e);
            throw e;
        }
    }

    /**
     * Изменение задачи по коду
     */
    public void updateTaskById() {
        logger.info("[Update task by id]");
        System.out.println("Введите код задачи: ");
        final Long vId = Long.getLong(scanner.nextLine());
        System.out.println("Введите новое название задачи: ");
        final String name = scanner.nextLine();
        System.out.println("Введите новое описание задачи: ");
        final String description = scanner.nextLine();
        if (update(vId, name, description) != null) {
            TaskManagerUtil.printAndReturnOk();
        } else {
            TaskManagerUtil.printAndReturnFail("Изменение задачи не удалось!");
        }
    }

    /**
     * Возвращает размер коллекции
     *
     * @return число, определяющее размер коллекции
     */
    public int size() {
        return taskRepository.size();
    }

    /**
     * Список задач, привязанных к проекту
     *
     * @param projectId код проекта
     * @return список типа {@link List}
     * @see Task
     */
    private List<Task> findAllByProjectId(final Long projectId) {
        if (projectId == null) return Collections.emptyList();
        return taskRepository.findAllByProjectId(projectId);
    }


    /**
     * Удаление задачи из проекта
     *
     * @param taskId код задачи
     * @return задача, удаленная из проекта
     * @see Task
     */
    private Task removeTask(final Long taskId) {
        if (taskId == null) return null;
        return taskRepository.removeTaskFromProject(taskId);
    }

    /**
     * Просмотр задачи в консоли
     *
     * @param task задача {@link Task}
     */
    public void viewTask(final Task task) {
        if (task == null) return;
        logger.info("[View Task]");
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("PROJECT_ID: " + task.getProjectId());
        logger.info("[OK]");
    }

    /**
     * Просмотр задачи по индексу
     */
    public void viewTaskByIndex() throws TaskNotFoundException {
        System.out.println("Введите индекс задачи: ");
        final int index = scanner.nextInt() - 1;
        final Task task;
        try {
            task = findByIndex(index);
            viewTask(task);
            TaskManagerUtil.printAndReturnOk();
        } catch (TaskNotFoundException e) {
            logger.error(e);
            throw e;
        }
    }

    /**
     * Создание задачи
     */
    public void createTask() {
        logger.info("[CREATE TASK]");
        System.out.println("Укажите имя задачи: ");
        final String lv_name = scanner.nextLine();
        create(lv_name);
        TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Очистка задачи
     */
    public void clearTask() {
        logger.info("[CLEAR TASK]");
        clear();
        TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Список задач
     */
    public void listTask() {
        logger.info("[LIST TASK]");
        System.out.println(findAll());
        TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Поиск задачи по ID
     *
     * @param taskId код задачи
     * @return задача {@link Task}
     */
    public Task findTaskById(final Long taskId) {
        if (taskId == null) return null;
        return findById(taskId);
    }

    /**
     * Список задач, привязанных к проекту
     */
    public void findAllByProjectId() {
        logger.info("[VIEW TASKS BY PROJECT ID]");
        System.out.println("Введите код проекта: ");
        final Long projectId = Long.parseLong(scanner.nextLine());
        if (projectId == null) return;
        List<Task> tasks = findAllByProjectId(projectId);
        if (tasks.isEmpty()) {
            TaskManagerUtil.printAndReturnFail("Задач для проекта не найдено");
        } else {
            tasks.forEach(this::viewTask);
        }
        TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Список задач, непривязанных к проектам
     */
    public void findAllWithoutProject() {
        logger.info("[VIEW TASKS WITHOUT PROJECT]");
        List<Task> tasks = taskRepository.findAllWithoutProject();
        if (tasks.isEmpty()) {
            TaskManagerUtil.printAndReturnFail("Задач, непривязанных к проектам не найдено");
        } else {
            tasks.forEach(this::viewTask);
        }
        TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Удаление задачи из проекта
     *
     * @param taskId код задачи
     */
    public void removeTaskFromProject(final Long taskId) {
        if (taskId == null) return;
        removeTask(taskId);
        TaskManagerUtil.printAndReturnOk();
    }

}

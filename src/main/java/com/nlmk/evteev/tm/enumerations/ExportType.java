package com.nlmk.evteev.tm.enumerations;

/**
 * Перечисление для типов выгрузки в файл
 */
public enum ExportType {
    TYPE_JSON("Формат JSON"),
    TYPE_XML("Формат XML");

    private final String displayName;

    ExportType(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

    @Override
    public String toString() {
        return getDisplayName();
    }

}

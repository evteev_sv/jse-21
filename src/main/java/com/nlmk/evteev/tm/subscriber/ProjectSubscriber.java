package com.nlmk.evteev.tm.subscriber;

import com.nlmk.evteev.tm.enumerations.ExportType;
import com.nlmk.evteev.tm.exceptions.ProjectNotFoundException;
import com.nlmk.evteev.tm.exceptions.TaskNotFoundException;
import com.nlmk.evteev.tm.service.ProjectService;
import com.nlmk.evteev.tm.service.ProjectTaskService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.nlmk.evteev.tm.constant.TerminalConst.*;

public class ProjectSubscriber implements ISubscriber {

    private static final Logger logger = LogManager.getLogger(ProjectSubscriber.class);

    @Override
    public void handleNotify(String msg) {
        if (msg == null || msg.isEmpty()) {
            logger.warn("Команда пустая. Выполнение прервано.");
            return;
        }
        switch (msg) {
            case PROJECT_CREATE:
                ProjectService.getInstance().createProject();
                break;
            case PROJECT_CLEAR:
                ProjectService.getInstance().clearProject();
                break;
            case PROJECT_LIST:
                ProjectService.getInstance().listProject();
                break;
            case PROJECT_VIEW:
                try {
                    ProjectService.getInstance().viewProjectByIndex();
                } catch (ProjectNotFoundException e) {
                    logger.error("Проект по индексу не найден! ".concat(e.getMessage()));
                }
                break;
            case PROJECT_VIEW_WITH_TASKS:
                try {
                    ProjectTaskService.getInstance().viewProjectWithTasks();
                } catch (ProjectNotFoundException e) {
                    logger.error("Ошибка вывода проекта! ".concat(e.getMessage()));
                }
                break;
            case PROJECT_REMOVE_BY_ID:
                ProjectService.getInstance().removeProjectById();
                break;
            case PROJECT_REMOVE_BY_NAME:
                ProjectService.getInstance().removeProjectByName();
                break;
            case PROJECT_REMOVE_BY_INDEX:
                try {
                    ProjectService.getInstance().removeProjectByIndex();
                } catch (ProjectNotFoundException e) {
                    logger.error("Удаление проекта не удалось! ".concat(e.getMessage()));
                }
                break;
            case PROJECT_UPDATE_BY_INDEX:
                try {
                    ProjectService.getInstance().updateProjectByIndex();
                } catch (ProjectNotFoundException e) {
                    logger.error("Обновление проекта не удалось! ".concat(e.getMessage()));
                }
                break;
            case PROJECT_UPDATE_BY_ID:
                ProjectService.getInstance().updateProjectById();
                break;
            case PROJECT_ADD_TASK_BY_IDS:
                try {
                    ProjectTaskService.getInstance().addTaskToProjectByIds();
                } catch (ProjectNotFoundException | TaskNotFoundException e) {
                    logger.error("Ошибка добавления задачи! ".concat(e.getMessage()));
                }
                break;
            case PROJECT_REMOVE_TASK_BY_IDS:
                try {
                    ProjectTaskService.getInstance().removeTaskFromProjectById();
                } catch (ProjectNotFoundException | TaskNotFoundException e) {
                    logger.error("Ошибка удаления задачи! ".concat(e.getMessage()));
                }
                break;
            case PROJECT_REMOVE_TASKS:
                ProjectTaskService.getInstance().clearProjectTasks();
                break;
            case PROJECT_REMOVE_WITH_TASKS:
                ProjectTaskService.getInstance().removeProjectWithTasks();
                break;
            case PROJECT_CHANGE_OWNER:
                ProjectService.getInstance().assignProjectToUser();
                break;
            case PROJECT_EXPORT_JSON:
                ProjectTaskService.getInstance().exportData(ExportType.TYPE_JSON);
                break;
            case PROJECT_EXPORT_XML:
                ProjectTaskService.getInstance().exportData(ExportType.TYPE_XML);
                break;
        }
    }

}

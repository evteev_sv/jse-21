package com.nlmk.evteev.tm.export;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.google.gson.Gson;
import com.nlmk.evteev.tm.enumerations.ExportType;
import com.nlmk.evteev.tm.repository.AbstractRepository;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;

public class DataExporter<T> extends AbstractExporter<T> {

    public DataExporter(ExportType exportType, AbstractRepository<T> repository) {
        super(exportType, repository);
    }

    @Override
    public void exportToFile(String fileName) throws IOException {
        File file = new File(filePath.concat(fileName));
        if (!file.exists()) {
            file.getParentFile().mkdir();
        }
        switch (getExportType()) {
            case TYPE_JSON:
                exportJson(file);
                break;
            case TYPE_XML:
                exportXml(file);
                break;
        }
    }

    /**
     * Экспорт данных в формат JSON
     *
     * @param file файл, в который происходит экспорт
     * @throws IOException ошибка при экспорте в файл
     */
    private void exportJson(File file) throws IOException {
        List<T> list = getAbstractRepository().findAll();
        Gson gson = new Gson();
        FileOutputStream outputStream = new FileOutputStream(file);
        byte[] strToBytes = gson.toJson(list).getBytes(Charset.defaultCharset());
        outputStream.write(strToBytes);
        outputStream.close();
    }

    /**
     * Экспорт данных в формат XML
     *
     * @param file файл, в который происходит экспорт
     * @throws IOException ошибка при экспорте в файл
     */
    private void exportXml(File file) throws IOException {
        List<T> list = getAbstractRepository().findAll();
        XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.writeValue(file, list);
    }

}

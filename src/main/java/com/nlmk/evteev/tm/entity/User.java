package com.nlmk.evteev.tm.entity;

import com.nlmk.evteev.tm.enumerations.UserRole;
import com.nlmk.evteev.tm.utils.TaskManagerUtil;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.UUID;

/**
 * Класс, описывающий пользователя
 */
@XmlRootElement
public class User {

    private final UUID userId = UUID.randomUUID();
    private String firstName = "";
    private String middleName = "";
    private String secondName = "";
    private String loginName = "";
    private String password;
    private UserRole userRole = UserRole.USER;

    public User() {
    }

    /**
     * Конструктор
     *
     * @param firstName  фамилия
     * @param secondName отчество
     * @param middleName имя
     * @param loginName  регистрационное имя
     * @param password   пароль
     * @param userRole   роль пользователя {@link UserRole}
     */
    public User
    (final String loginName, final String password, final UserRole userRole,
     final String firstName, final String middleName, final String secondName
    ) {
        this.firstName = firstName;
        this.middleName = middleName;
        this.secondName = secondName;
        this.loginName = loginName;
        this.password = TaskManagerUtil.getMD5Hash(password);
        this.userRole = userRole;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = TaskManagerUtil.getMD5Hash(password);
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }

    public boolean isAdmin() {
        return userRole == UserRole.ADMIN;
    }

    public UUID getUserId() {
        return userId;
    }

    @Override
    public String toString() {
        return String.format("user: [UUID = %s, \nfirstName = %s,\n middleName = %s,\n secondName = %s,\nloginName = %s, " +
                        "\n userRole = %s, \n password = %h]",
                userId, firstName, middleName, secondName, loginName, userRole, password);
    }

}

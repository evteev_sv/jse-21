package com.nlmk.evteev.tm.repository;

import com.nlmk.evteev.tm.entity.Project;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Класс для работы с объектом проект
 * {@link com.nlmk.evteev.tm.entity.Project}
 */
public class ProjectRepository extends AbstractRepository<Project> {

    private static ProjectRepository instance = null;

    private ProjectRepository() {
    }

    /**
     * Создает и возвращает экземпляр класса репозитория проекта
     *
     * @return репозиторий проекта {@link ProjectRepository}
     */
    public static ProjectRepository getInstance() {
        synchronized (ProjectRepository.class) {
            return instance == null
                    ? instance = new ProjectRepository()
                    : instance;
        }
    }

    /**
     * Создание проекта и добавление его в список проектов
     *
     * @param name имя проекта
     * @return объект типа {@link Project}
     */
    public Project create(final String name, final UUID userId) {
        final Project project = new Project(name);
        if (userId != null)
            project.setOwnedUser(userId);
        create(project);
        return project;
    }

    /**
     * Очистка списка проектов
     */
    public void clear() {
        itemsList.clear();
    }

    /**
     * Очистка списка на основании другого списка проектов
     *
     * @param projectList список проектов для очистки
     */
    public void clearListProject(final List<Project> projectList) {
        itemsList.removeAll(projectList);
    }

    /**
     * Поиск проекта по индексу
     *
     * @param index индекс проекта
     * @return проект {@link Project}
     */
    @Override
    public Project findByIndex(final int index) {
        if (index > size())
            return null;
        return itemsList.get(index);
    }

    /**
     * Поиск проекта по наименованию
     *
     * @param name наименование проекта
     * @return проект {@link Project}
     */
    @Override
    public Project findByName(final String name) {
        for (Project project : itemsList) {
            if (project.getName().equals(name))
                return project;
        }
        return null;
    }

    /**
     * Поиск проекта по коду
     *
     * @param id код проекта
     * @return проект {@link Project}
     */
    @Override
    public Project findById(final Number id) {
        for (Project project : itemsList) {
            if (project.getId().equals(id))
                return project;
        }
        return null;
    }

    /**
     * Поиск проектов по ID пользователя
     *
     * @param userId код пользователя
     * @return список пользовательских проектов {@link List}
     * @see Project
     */
    public List<Project> findByUserId(final UUID userId) {
        return itemsList.stream().filter(project -> project.getOwnedUser().equals(userId)).collect(Collectors.toList());
    }

    /**
     * Удаление проекта по коду
     *
     * @param id код проекта
     * @return проект, который был удален {@link Project}
     */
    @Override
    public Project removeById(final Number id) {
        final Project project = findById(id);
        if (project == null) return null;
        remove(project);
        return project;
    }

    /**
     * Удален епроекта по индексу
     *
     * @param index индекс проекта
     * @return проект, который был удален {@link Project}
     */
    @Override
    public Project removeByIndex(final int index) {
        final Project project = findByIndex(index);
        if (project == null) return null;
        remove(project);
        return project;
    }

    /**
     * Удаление проекта по названию.
     *
     * @param name название проекта
     * @return проект, который был удален {@link Project}
     */
    @Override
    public Project removeByName(final String name) {
        final Project project = findByName(name);
        if (project == null) return null;
        remove(project);
        return project;
    }

    /**
     * Изменение проекта
     *
     * @param id          код проекта
     * @param name        наименование проекта
     * @param description описание проекта
     * @return проект, который изменен {@link Project}
     */
    public Project update(final Long id, final String name, final String description) {
        Project project = findById(id);
        if (project == null)
            return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    /**
     * Изменение владельца проекта
     *
     * @param projectId код проекта
     * @param userId    код владельца
     * @return проект {@link Project}
     */
    public Project updateProjectOwner(final Long projectId, final UUID userId) {
        Project project = findById(projectId);
        if (project == null) return null;
        project.setOwnedUser(userId);
        return project;
    }

}

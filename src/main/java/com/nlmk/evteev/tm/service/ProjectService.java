package com.nlmk.evteev.tm.service;

import com.nlmk.evteev.tm.entity.Project;
import com.nlmk.evteev.tm.exceptions.ProjectNotFoundException;
import com.nlmk.evteev.tm.repository.ProjectRepository;
import com.nlmk.evteev.tm.utils.TaskManagerUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Scanner;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Сервисный класс для работы с репозиторием проектов
 */
public class ProjectService {

    private final static Logger logger = LogManager.getLogger(ProjectService.class);

    private static final ProjectRepository projectRepository = ProjectRepository.getInstance();
    private static ProjectService instance = null;
    protected final Scanner scanner = new Scanner(System.in);

    private ProjectService() {
    }

    /**
     * Создает и возвращает экземпляр класса
     *
     * @return экземпляр класса {@link ProjectService}
     */
    public static ProjectService getInstance() {
        synchronized (ProjectService.class) {
            return instance == null
                    ? instance = new ProjectService()
                    : instance;
        }
    }

    /**
     * Создание проекта и добавление его в список проектов
     *
     * @param name   имя проекта
     * @param userId код пользователя {@link UUID}
     * @return объект типа {@link Project}
     */
    private Project create(final String name, final UUID userId) {
        if (name == null || name.isEmpty())
            return null;
        logger.trace("Создание проекта с названием " + name);
        return projectRepository.create(name, userId);
    }

    /**
     * Очистка списка проектов
     */
    private void clear() {
        projectRepository.clear();
    }

    /**
     * Очистка списка на основании другого списка проектов
     *
     * @param projectList список проектов для очистки {@link List}
     */
    private void clearListProject(List<Project> projectList) {
        projectRepository.clearListProject(projectList);
    }

    /**
     * Возвращает список проектов
     *
     * @return список проектов {@link List}
     */
    private List<Project> findAll() {
        return projectRepository.findAll();
    }

    /**
     * Поиск проекта по индексу
     *
     * @param index индекс проекта
     * @return проект {@link Project}
     * @throws ProjectNotFoundException ошибка при ненахождении проекта
     */
    private Project findByIndex(final int index) throws ProjectNotFoundException {
        if (index < 0) return null;
        Project project = projectRepository.findByIndex(index);
        if (project == null) {
            logger.error("Проект по данному индексу не найден!");
            throw new ProjectNotFoundException("Проект по данному индексу не найден!");
        }
        return project;
    }

    /**
     * Поиск проекта по наименованию
     *
     * @param name наименование проекта
     * @return проект {@link Project}
     */
    private Project findByName(final String name) {
        if (name == null || name.isEmpty())
            return null;
        logger.trace("Поиск проекта с названием " + name);
        return projectRepository.findByName(name);
    }

    /**
     * Поиск проектов по ID пользователя
     *
     * @param userId код пользователя
     * @return список пользовательских проектов {@link List}
     * @see Project
     */
    private List<Project> findByUserId(UUID userId) {
        logger.trace("Поиск проектов по пользователю " + userId.toString());
        return projectRepository.findByUserId(userId);
    }

    /**
     * Поиск проекта по коду
     *
     * @param id код проекта
     * @return проект {@link Project}
     */
    private Project findById(final Long id) {
        if (id == null) return null;
        return projectRepository.findById(id);
    }

    /**
     * Удаление проекта по коду
     *
     * @param id код проекта
     * @return проект, который был удален {@link Project}
     */
    public Project removeById(final Long id) {
        if (id == null) return null;
        return projectRepository.removeById(id);
    }

    /**
     * Удален епроекта по индексу
     *
     * @param index индекс проекта
     * @return проект, который был удален {@link Project}
     */
    private Project removeByIndex(final int index) {
        if (index < 0 || index > projectRepository.size() - 1) return null;
        return projectRepository.removeByIndex(index);
    }

    /**
     * Удаление проекта по названию.
     *
     * @param name название проекта
     * @return проект, который был удален {@link Project}
     */
    private Project removeByName(final String name) {
        if (name == null || name.isEmpty())
            return null;
        return projectRepository.removeByName(name);
    }

    /**
     * Изменение проекта
     *
     * @param id          код проекта
     * @param name        наименование проекта
     * @param description описание проекта
     * @return проект, который изменен {@link Project}
     */
    private Project update(final Long id, final String name, final String description) {
        if (id == null || name == null || name.isEmpty() || description == null)
            return null;
        return projectRepository.update(id, name, description);
    }

    /**
     * Изменение владельца проекта
     *
     * @param projectId код проекта
     * @param userId    код владельца
     * @return проект {@link Project}
     */
    private Project updateProjectOwner(Long projectId, UUID userId) {
        if (projectId == null || userId == null) return null;
        return projectRepository.updateProjectOwner(projectId, userId);
    }


    /**
     * Удаление проекта по имени
     */
    public void removeProjectByName() {
        logger.info("[Remove project by name]");
        System.out.println("Введите имя проекта: ");
        final String name = scanner.nextLine();
        if (!TaskManagerUtil.checkEmptyInput(name)) {
            TaskManagerUtil.printAndReturnFail("Название проекта не может быть пустым.");
            return;
        }
        Project project = findByName(name);
        if (!TaskManagerUtil.checkProjectPrivs(project)) {
            TaskManagerUtil.printAndReturnFail("Проект с таким названием не принадлежит пользователю.");
            return;
        }
        project = removeByName(name);
        if (project == null) {
            TaskManagerUtil.printAndReturnFail("Проект с таким названием не найден.");
            return;
        }
        TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Удаление проекта по коду
     */
    public void removeProjectById() {
        System.out.println("[Remove project by id]");
        System.out.println("Введите ID проекта: ");
        final Long vId = scanner.nextLong();
        Project project = findById(vId);
        if (!TaskManagerUtil.checkProjectPrivs(project)) {
            TaskManagerUtil.printAndReturnFail("Проект с таким ID не принадлежит пользователю.");
            return;
        }
        project = removeById(vId);
        if (project == null)
            TaskManagerUtil.printAndReturnFail("Проекта с таким ID не найдено!");
        TaskManagerUtil.printAndReturnOk();
    }


    /**
     * Удаление проекта по индексу
     */
    public void removeProjectByIndex() throws ProjectNotFoundException {
        logger.info("[Remove project by index]");
        System.out.println("Введите индекс проекта: ");
        final int index = scanner.nextInt();
        Project project;
        try {
            project = findByIndex(index);
            if (!TaskManagerUtil.checkProjectPrivs(project)) {
                TaskManagerUtil.printAndReturnFail("Проект с таким ID не принадлежит пользователю.");
                return;
            }
            removeByIndex(index);
        } catch (ProjectNotFoundException e) {
            logger.error(e);
            throw e;
        }
        TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Изменение проекта по коду
     */
    public void updateProjectById() {
        logger.info("[Update project by id]");
        System.out.println("Введите код проекта: ");
        final Long vId = Long.getLong(scanner.nextLine());
        System.out.println("Введите новое имя проекта: ");
        final String name = scanner.nextLine();
        System.out.println("Введите новое описание проекта: ");
        final String description = scanner.nextLine();
        if (update(vId, name, description) == null) {
            logger.error("Ошибка обновления проекта. ");
            TaskManagerUtil.printAndReturnFail("Ошибка обновления проекта.");
            return;
        }
        TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Изменение проекта по индексу
     */
    public void updateProjectByIndex() throws ProjectNotFoundException {
        logger.info("[Update project by index]");
        System.out.println("Введите индекс проекта: ");
        final int vId = Integer.parseInt(scanner.nextLine()) - 1;
        final Project project;
        try {
            project = findByIndex(vId);
            System.out.println("Введите новое имя проекта: ");
            final String name = scanner.nextLine();
            System.out.println("Введите новое описание проекта: ");
            final String description = scanner.nextLine();
            if (update(project.getId(), name, description) == null) {
                TaskManagerUtil.printAndReturnFail("Изменение проекта не удалось!");
                return;
            }
        } catch (ProjectNotFoundException e) {
            logger.error(e);
            throw e;
        }
        TaskManagerUtil.printAndReturnOk();
    }


    /**
     * Просмотр проекта по индексу
     */
    public void viewProjectByIndex() throws ProjectNotFoundException {
        System.out.println("Введите индекс проекта: ");
        final int index = scanner.nextInt() - 1;
        final Project project;
        try {
            project = findByIndex(index);
            viewProject(project);
        } catch (ProjectNotFoundException e) {
            logger.error(e);
            throw e;
        }
    }

    /**
     * Просмотр проекта в консоли
     *
     * @param project проект {@link Project}
     */
    public void viewProject(final Project project) {
        if (project == null) {
            TaskManagerUtil.printAndReturnFail("Проект не определен!");
            return;
        }
        System.out.println("[View Project]");
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.print("OWNER: ");
        if (project.getOwnedUser() != null) {
            UserService.getInstance().viewUserById(project.getOwnedUser().toString());
        } else {
            System.out.println("не назначен.");
        }
        TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Поиск проекта по коду
     *
     * @param projectId код проекта
     * @return проект {@link Project}
     */
    public Project findProjectById(final Long projectId) {
        if (projectId == null) {
            return null;
        }
        Project project = findById(projectId);
        if (!TaskManagerUtil.checkProjectPrivs(project)) {
            return null;
        }
        return project;
    }

    /**
     * Создание проекта
     */
    public void createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("Укажите имя проекта: ");
        final String lv_name = scanner.nextLine();
        if (!TaskManagerUtil.checkEmptyInput(lv_name)) {
            TaskManagerUtil.printAndReturnFail("Имя проекта не может быть пустым!");
            return;
        }

        create(lv_name, UserService.getInstance().getAppUser().getUserId());
        TaskManagerUtil.printAndReturnOk();
    }

    public void createProject(final String projectName) {
        if (!TaskManagerUtil.checkEmptyInput(projectName)) {
            TaskManagerUtil.printAndReturnFail("Имя проекта не может быть пустым!");
            return;
        }
        create(projectName, null);
        TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Очистка проекта
     */
    public void clearProject() {
        System.out.println("[CLEAR PROJECT]");
        if (!UserService.getInstance().getAppUser().isAdmin()) {
            List<Project> projectList = findByUserId(UserService.getInstance().getAppUser().getUserId());
            if (projectList.isEmpty()) {
                TaskManagerUtil.printAndReturnFail("У данного пользователя нет ни одного проекта.");
                return;
            }
            clearListProject(projectList);
            TaskManagerUtil.printAndReturnOk();
        }
        clear();
        TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Список проектов
     */
    public void listProject() {
        System.out.println("[LIST PROJECT]");
        List<Project> projectList = findAll();
        if (!UserService.getInstance().getAppUser().isAdmin())
            projectList = findAll().stream().filter(TaskManagerUtil::checkProjectPrivs).collect(Collectors.toList());
        System.out.println(projectList);
        TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Добавление задачи к проекту
     *
     * @param projectId код проекта
     * @param taskId    код задачи
     */
    public void addTaskToProject(final Long projectId, final Long taskId) {
        if (projectId == null) {
            TaskManagerUtil.printAndReturnFail("Код проекта не определен.");
            return;
        }
        if (taskId == null) {
            TaskManagerUtil.printAndReturnFail("Код задачи не определен.");
            return;
        }
        Project project = findById(projectId);
        if (project == null) {
            TaskManagerUtil.printAndReturnFail("Проект по коду не найден.");
            return;
        }
        if (!TaskManagerUtil.checkProjectPrivs(project)) {
            TaskManagerUtil.printAndReturnFail("Проект не принадлежит данному пользователю.");
            return;
        }
        project.getTasks().add(taskId);
        TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Удаление задачи из проекта
     *
     * @param projectId код проекта
     * @param taskId    код задачи
     */
    public void removeTaskFromProject(final Long projectId, final Long taskId) {
        if (projectId == null) {
            TaskManagerUtil.printAndReturnFail("Код проекта должен быть не пустой.");
            return;
        }
        if (taskId == null) {
            TaskManagerUtil.printAndReturnFail("Код задачи не определен.");
            return;
        }
        Project project = findById(projectId);
        if (project == null) {
            TaskManagerUtil.printAndReturnFail("Проект по коду не найден.");
            return;
        }
        if (!TaskManagerUtil.checkProjectPrivs(project)) {
            TaskManagerUtil.printAndReturnFail("Проект не принадлежит данному пользователю.");
            return;
        }
        project.getTasks().remove(taskId);
        TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Назначение пользователя на проект
     */
    public void assignProjectToUser() {
        if (!UserService.getInstance().getAppUser().isAdmin()) {
            TaskManagerUtil.printAndReturnFail("Вы не имеете прав на смену владельца для проекта!");
            return;
        }
        System.out.println("Введите код проекта:");
        final Long projectId = Long.parseLong(scanner.nextLine());
        if (projectId == null) {
            TaskManagerUtil.printAndReturnFail("Код проекта не верен!");
            return;
        }
        System.out.println("Введите код нового владельца проекта:");
        UUID uuid = UUID.fromString(scanner.nextLine());
        if (uuid == null) {
            TaskManagerUtil.printAndReturnFail("Код пользователя не верен!");
            return;
        }
        updateProjectOwner(projectId, uuid);
        TaskManagerUtil.printAndReturnOk();
    }

}

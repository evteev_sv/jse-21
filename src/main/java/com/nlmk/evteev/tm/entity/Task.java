package com.nlmk.evteev.tm.entity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Класс, описывающий задачи
 */
@XmlRootElement
public class Task {

    private Long id = System.nanoTime();
    private String name;
    private String description = "";
    private Long projectId;

    /**
     * Конструктор класса
     *
     * @param pName название задачи
     */
    public Task(String pName) {
        name = pName;
    }

    public Long getId() {
        return id;
    }

    @XmlElement
    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    @XmlElement
    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    @XmlElement
    public void setDescription(String description) {
        this.description = description;
    }

    public Long getProjectId() {
        return projectId;
    }

    @XmlElement
    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    @Override
    public String toString() {
        return "Task {id = " + id + ", name = " + name + ", projectId = " + projectId + "}";
    }

}

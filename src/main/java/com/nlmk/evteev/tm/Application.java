package com.nlmk.evteev.tm;

import com.nlmk.evteev.tm.enumerations.UserRole;
import com.nlmk.evteev.tm.repository.UserRepository;
import com.nlmk.evteev.tm.service.ProjectService;
import com.nlmk.evteev.tm.subscriber.ProjectSubscriber;
import com.nlmk.evteev.tm.subscriber.Publisher;
import com.nlmk.evteev.tm.subscriber.SystemSubscriber;
import com.nlmk.evteev.tm.subscriber.TaskSubscriber;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Calendar;

/**
 * Основной класс
 */
public class Application {

    private final static Logger logger = LogManager.getLogger(Application.class);
    private final static Publisher publisher = Publisher.getInstance();

    static {
        UserRepository.getInstance().create("ivanov", "qwerty", UserRole.ADMIN,
                "Иванов", "Иван", "Иванович");
        UserRepository.getInstance().create("petrov", "asdzxc", UserRole.USER,
                "Петров", "Петр", "Петрович");
        ProjectService.getInstance().createProject("Проект № 1");
        ProjectService.getInstance().createProject("Проект № 2");
    }

    /**
     * Точка входа
     *
     * @param args дополнительные аргументы запуска
     */
    public static void main(String[] args) {
        logger.debug("Start application at " + Calendar.getInstance().getTime());
        SystemSubscriber systemSubscriber = new SystemSubscriber();
        publisher.addSubscriber(systemSubscriber);
        TaskSubscriber taskSubscriber = new TaskSubscriber();
        publisher.addSubscriber(taskSubscriber);
        ProjectSubscriber projectSubscriber = new ProjectSubscriber();
        publisher.addSubscriber(projectSubscriber);
        publisher.initPublisher();
        logger.debug("Stop application at" + Calendar.getInstance().getTime());
    }

}

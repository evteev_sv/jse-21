package com.nlmk.evteev.tm.subscriber;

import com.nlmk.evteev.tm.service.SystemService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.nlmk.evteev.tm.constant.TerminalConst.*;

public class SystemSubscriber implements ISubscriber {

    private final static Logger logger = LogManager.getLogger(SystemSubscriber.class);

    @Override
    public void handleNotify(String msg) {
        if (msg == null || msg.isEmpty()) {
            logger.warn("Команда пустая. Выполнение прервано.");
            return;
        }
        switch (msg) {
            case CMD_VERSION:
                SystemService.getInstance().displayVersion();
                break;
            case CMD_HELP:
                SystemService.getInstance().displayHelp();
                break;
            case CMD_ABOUT:
                SystemService.getInstance().displayAbout();
                break;
            case CMD_EXIT:
                SystemService.getInstance().displayExit();
                break;
            case VIEW_HISTORY:
                SystemService.getInstance().displayHistory();
                break;
        }
    }
}

package com.nlmk.evteev.tm.export;

import com.nlmk.evteev.tm.enumerations.ExportType;
import com.nlmk.evteev.tm.repository.AbstractRepository;

import java.io.IOException;

public abstract class AbstractExporter<T> {

    protected final String filePath = "data/";
    private AbstractRepository<T> abstractRepository;
    private ExportType exportType;

    public AbstractExporter(ExportType exportType, AbstractRepository<T> repository) {
        this.exportType = exportType;
        this.abstractRepository = repository;
    }

    public ExportType getExportType() {
        return exportType;
    }

    public AbstractRepository<T> getAbstractRepository() {
        return abstractRepository;
    }

    public abstract void exportToFile(String fileName) throws IOException;

}

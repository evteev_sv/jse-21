package com.nlmk.evteev.tm.repository;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractRepository<T> {

    protected final List<T> itemsList = new ArrayList<>();

    public void create(T item) {
        if (!itemsList.contains(item))
            itemsList.add(item);
    }

    public void remove(T item) {
        itemsList.remove(item);
    }

    public List<T> findAll() {
        return itemsList;
    }

    public abstract T findById(final Number id);

    public abstract T findByName(final String name);

    public abstract T findByIndex(final int index);

    public abstract T removeById(final Number id);

    public abstract T removeByName(final String name);

    public abstract T removeByIndex(final int index);

    public int size() {
        return itemsList.size();
    }
}

package com.nlmk.evteev.tm.utils;

import com.nlmk.evteev.tm.entity.Project;
import com.nlmk.evteev.tm.service.UserService;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Класс-утилита
 */
public class TaskManagerUtil {

    private static final UserService userService = UserService.getInstance();

    /**
     * Получение hash пароля
     *
     * @param str пароль
     * @return сроковое представление hash
     */
    public static String getMD5Hash(final String str) {
        if (str == null || str.isEmpty()) return "";
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("MD5");
            md.update(str.getBytes());
            return new String(md.digest());
        } catch (NoSuchAlgorithmException e) {
            return "";
        }
    }

    /**
     * Проверка вводимого значения на NULL или пустое значение.
     *
     * @param inputString вводимая строка
     * @return истина, если строка не пустая, иначе ложь
     */
    public static boolean checkEmptyInput(final String inputString) {
        return inputString != null && !inputString.isEmpty();
    }

    /**
     * Проверка на привилегий на проект
     *
     * @param project проект {@link Project}
     * @return истина, если проект принадлежит пользователю, или пользователь является администратором,
     * иначе - ложь
     */
    public static boolean checkProjectPrivs(final Project project) {
        if (project == null) {
            return false;
        }
        if (project.getOwnedUser() == null) {
            return userService.getAppUser().isAdmin();
        }
        if (project.getOwnedUser() != null) {
            return project.getOwnedUser().equals(userService.getAppUser().getUserId())
                    || userService.getAppUser().isAdmin();
        }
        return false;
    }

    /**
     * Вывод сообщения о неверных действиях
     *
     * @param whatPrint текст сообщения
     * @return код исполнения
     */
    public static int printAndReturnFail(final String whatPrint) {
        System.out.println(whatPrint);
        System.out.println("[FAIL]");
        return -1;
    }

    /**
     * Вывод безошибочного сообщения
     *
     * @return код исполнения
     */
    public static int printAndReturnOk() {
        System.out.println("[OK]");
        return 0;
    }

}

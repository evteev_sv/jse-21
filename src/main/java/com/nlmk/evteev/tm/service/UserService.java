package com.nlmk.evteev.tm.service;

import com.nlmk.evteev.tm.entity.User;
import com.nlmk.evteev.tm.enumerations.UserRole;
import com.nlmk.evteev.tm.repository.UserRepository;
import com.nlmk.evteev.tm.utils.TaskManagerUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Scanner;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Сервисный класс для работы с репозиторием пользователя
 */
public class UserService {

    private static final Logger logger = LogManager.getLogger(UserService.class);
    private static final UserRepository userRepository = UserRepository.getInstance();
    private static UserService instance = null;
    protected final Scanner scanner = new Scanner(System.in);
    private User appUser;

    private UserService() {
    }

    /**
     * Создает и возвращает экземпляр класса
     *
     * @return экземпляр класса {@link UserService}
     */
    public static UserService getInstance() {
        synchronized (UserService.class) {
            return instance == null
                    ? instance = new UserService()
                    : instance;
        }
    }

    public User getAppUser() {
        return appUser;
    }

    public void setAppUser(User appUser) {
        this.appUser = appUser;
    }

    /**
     * Запрос на предоставление прав адинистратора
     *
     * @return роль пользователя {@link UserRole}
     */
    private UserRole addAdminRole() {
        System.out.println("Предоставить административные права? (y - да, n - нет):");
        final String s = scanner.nextLine();
        if (!TaskManagerUtil.checkEmptyInput(s)) return UserRole.USER;
        return s.trim().toLowerCase().equals("y") ? UserRole.ADMIN : UserRole.USER;
    }

    /**
     * Создание пользователя
     *
     * @return созданный в системе пользователь {@link User}
     */
    public User createUser() {
        logger.info("[CREATE USER]");
        System.out.println("Введите регистрационное имя:");
        final String loginName = scanner.nextLine();
        if (!TaskManagerUtil.checkEmptyInput(loginName)) {
            logger.error("Регистрационное имя не может быть пустым!");
            TaskManagerUtil.printAndReturnFail("Регистрационное имя не может быть пустым!");
            return null;
        }
        if (getUserByLoginName(loginName) != null) {
            logger.error("Пользователь с таким регистрационным именем уже существует!");
            TaskManagerUtil.printAndReturnFail("Пользователь с таким регистрационным именем уже существует!");
            return null;
        }
        System.out.println("Введите пароль:");
        final String password = scanner.nextLine();
        if (!TaskManagerUtil.checkEmptyInput(password)) {
            logger.error("Пароль не может быть пустым!");
            TaskManagerUtil.printAndReturnFail("Пароль не может быть пустым!");
            return null;
        }
        final UserRole userRole = addAdminRole();
        return userRepository.create(loginName, password, userRole);
    }

    /**
     * Ввод данных о пользователе
     *
     * @param user пользователь
     */
    private void inputFIO(User user) {
        System.out.println("Введите фамилию пользователя:");
        String firstName = scanner.nextLine();
        if (TaskManagerUtil.checkEmptyInput(firstName) && !firstName.equals(user.getFirstName()))
            user.setFirstName(firstName);
        System.out.println("Введите имя пользователя:");
        String middleName = scanner.nextLine();
        if (!TaskManagerUtil.checkEmptyInput(middleName) && !middleName.equals(user.getMiddleName()))
            user.setMiddleName(middleName);
        System.out.println("Введите отчество пользователя:");
        String secondName = scanner.nextLine();
        if (!TaskManagerUtil.checkEmptyInput(secondName) && !secondName.equals(user.getSecondName()))
            user.setSecondName(secondName);
        System.out.println("Введите новый пароль пользователя:");
        String password = scanner.nextLine();
        if (!TaskManagerUtil.checkEmptyInput(password)) {
            TaskManagerUtil.printAndReturnFail("Пароль не может быть пустым!");
            return;
        }
        user.setPassword(password);
    }


    /**
     * Изменение пользователя
     *
     * @return пользователь {@link User}
     */
    public User updateUser() {
        logger.info("[UPDATE USER DATA]");
        System.out.println("Введите регистрационное имя пользователя:");
        final String loginName = scanner.nextLine();
        User user = getUserByLoginName(loginName);
        if (user == null) {
            logger.error("Пользователя с таким регистрационным именем не существует: ".concat(loginName));
            TaskManagerUtil.printAndReturnFail("Пользователя с таким регистрационным именем не существует.");
            return null;
        }
        inputFIO(user);
        final UserRole userRole = addAdminRole();
        user.setUserRole(userRole);
        if (checkUserCredentials(loginName, user.getPassword(), userRole, user.getFirstName(), user.getMiddleName(), user.getSecondName()))
            return null;
        return userRepository.update(loginName, user.getPassword(), userRole, user.getFirstName(), user.getMiddleName(), user.getSecondName());
    }

    /**
     * Обновление пользователя
     *
     * @param user сущность пользователя для обновления
     * @return пользователь
     * @see User
     */
    private User updateUser(final User user) {
        if (user == null) return null;
        return userRepository.update(user);
    }

    /**
     * Удаление пользователя
     *
     * @return удаленный пользователь {@link User}
     * или NULL, если такого не существует
     */
    public User deleteUser() {
        if (!getAppUser().isAdmin()) {
            logger.error(appUser.getLoginName().concat(" не имеет прав на удаление пользователей!"));
            return null;
        }
        System.out.println("Введите регистрационное имя пользователя:");
        final String loginName = scanner.nextLine();
        if (!TaskManagerUtil.checkEmptyInput(loginName)) {
            logger.error("Логин пользователя не моожет быть пустым!");
            return null;
        }
        return userRepository.deleteUserByLogin(loginName);
    }

    /**
     * Печать списка пользователей
     */
    public void printListUsers() {
        logger.info("[LIST USERS]");
        userRepository.findAll().forEach(System.out::println);
    }


    /**
     * Вход в систему пользователя
     */
    public void userLogin() {
        System.out.println("Введите имя пользователя:");
        final String loginName = scanner.nextLine();
        if (!TaskManagerUtil.checkEmptyInput(loginName)) {
            TaskManagerUtil.printAndReturnFail("Вход в систему не удался. Имя пользователя не введено.");
            return;
        }
        final User user = getUserByLoginName(loginName);
        if (user == null) {
            TaskManagerUtil.printAndReturnFail("Пользователя с таким логином не существует.");
            return;
        }
        System.out.println("Введите пароль:");
        final String userPassword = scanner.nextLine();
        if (!TaskManagerUtil.checkEmptyInput(userPassword)) {
            TaskManagerUtil.printAndReturnFail("Вход в систему не удался. Пароль не может быть пустым!");
            return;
        }
        if (user != null && user.getPassword() != null && !user.getPassword().equals(TaskManagerUtil.getMD5Hash(userPassword))) {
            TaskManagerUtil.printAndReturnFail("Вход в систему не удался. Пароль не верен.");
            return;
        }
        setAppUser(user);
        TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Просмотр пользователя по ID
     */
    public void viewUserById(final String userId) {
        logger.info("[VIEW_USER_BY_ID]");
        if (!TaskManagerUtil.checkEmptyInput(userId)) return;
        List<User> filterList = findAll().stream().filter(user1 ->
                user1.getUserId().equals(UUID.fromString(userId))).collect(Collectors.toList());
        if (filterList.isEmpty()) {
            logger.error("Пользователь с ID= " + userId + " не найден");
            TaskManagerUtil.printAndReturnFail("Пользователь с ID= " + userId + " не найден");
            return;
        }
        final User user = filterList.get(0);
        System.out.println(user);
        TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Просмотр профиля пользователя
     */
    public void viewUserProfile() {
        logger.info("[VIEW USER PROFILE]");
        if (getAppUser() == null)
            TaskManagerUtil.printAndReturnFail("Пользователь не определен!");
        if (!getAppUser().isAdmin()) {
            viewUserById(getAppUser().getUserId().toString());
            TaskManagerUtil.printAndReturnOk();
        }
        System.out.println("Введите ID пользователя");
        final String userId = scanner.nextLine();
        if (!TaskManagerUtil.checkEmptyInput(userId)) {
            viewUserById(getAppUser().getUserId().toString());
        } else
            viewUserById(userId);
        TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Смена пароля пользователя
     */
    public void changeUserPassword() {
        logger.info("[CHANGE USER PASSWORD]");
        if (getAppUser() == null) return;
        if (!getAppUser().isAdmin()) {
            changeUserPasswordById(getAppUser().getUserId().toString());
            TaskManagerUtil.printAndReturnOk();
        }
        System.out.println("Введите ID пользователя");
        final String userId = scanner.nextLine();
        if (!TaskManagerUtil.checkEmptyInput(userId)) {
            changeUserPasswordById(getAppUser().getUserId().toString());
        } else
            changeUserPasswordById(userId);
        TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Смена пароля по ID пользователя
     *
     * @param userId ID пользоваьеля
     */
    private void changeUserPasswordById(final String userId) {
        if (!TaskManagerUtil.checkEmptyInput(userId)) return;
        final User user = getUserById(userId);
        if (user == null) return;
        System.out.println("Введите новый пароль:");
        final String inputPasswordString = scanner.nextLine();
        if (!TaskManagerUtil.checkEmptyInput(inputPasswordString)) {
            TaskManagerUtil.printAndReturnFail("Пароль не может быть пустым!");
            return;
        }
        user.setPassword(inputPasswordString);
        userRepository.update(user);
    }

    /**
     * Отключение пользователя
     */
    public void userLogOut() {
        logger.info("Отключение пользователя...");
        setAppUser(null);
        TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Редактирование профиля пользователя
     */
    public void editUserProfile() {
        logger.info("[EDIT USER PROFILE]");
        if (getAppUser() == null) return;
        if (!getAppUser().isAdmin()) {
            inputFIO(getAppUser());
            userRepository.update(getAppUser());
            TaskManagerUtil.printAndReturnOk();
        }
        System.out.println("Введите код пользователя");
        final String userId = scanner.nextLine();
        if (!TaskManagerUtil.checkEmptyInput(userId)) {
            inputFIO(getAppUser());
            userRepository.update(getAppUser());
        } else {
            final User user = userRepository.getUserById(userId);
            if (user == null) return;
            inputFIO(user);
            userRepository.update(user);
        }
        TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Проверка вводимых данных
     *
     * @param firstName  фамилия
     * @param secondName отчество
     * @param middleName имя
     * @param loginName  логон пользователя
     * @param password   пароль
     * @param userRole   роль пользователя
     * @return истина, если данные корректны, иначе ложь
     */
    private boolean checkUserCredentials
    (
            final String loginName, final String password, final UserRole userRole,
            final String firstName, final String middleName, final String secondName
    ) {
        if (firstName == null || firstName.isEmpty()) return false;
        if (middleName == null || middleName.isEmpty()) return false;
        if (secondName == null || secondName.isEmpty()) return false;
        if (loginName == null || loginName.isEmpty()) return false;
        if (password == null || password.isEmpty()) return false;
        return userRole != null;
    }

    /**
     * Возвращает пользователя по его логину
     *
     * @param loginName логин пользователя
     * @return пользователь {@link User} или NULL, если не найден
     */
    private User getUserByLoginName(final String loginName) {
        if (loginName == null || loginName.isEmpty()) return null;
        return userRepository.getUserByLoginName(loginName);
    }

    /**
     * Удаление пользователя
     *
     * @param userId логин пользователя
     * @return удаленный пользователь {@link User}
     * или NULL, если такого не существует
     */
    private User deleteUserById(final String userId) {
        if (userId == null || userId.isEmpty()) return null;
        return userRepository.deleteUserById(userId);
    }

    /**
     * Поиск пользователя по ID
     *
     * @param userId код пользователя
     * @return пользователь с данным ID, либо NULL, если не найден
     */
    private User getUserById(final String userId) {
        if (userId == null || userId.isEmpty()) return null;
        return userRepository.getUserById(userId);
    }

    /**
     * Список пользователей
     *
     * @return список пользователей в системе {@link List}
     * @see User
     */
    private List<User> findAll() {
        return userRepository.findAll();
    }

}

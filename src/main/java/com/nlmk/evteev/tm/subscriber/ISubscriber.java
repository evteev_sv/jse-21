package com.nlmk.evteev.tm.subscriber;

public interface ISubscriber {

    void handleNotify(String msg);
}
